/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chang.assigment2nov11;

import java.sql.Timestamp;
import java.util.Objects;

/**
 *this class is for the table Surgical
 * @author cha_xi
 */
public class SurgicalBean {
    private int id;
    private int patientId;
    private Timestamp dateOfSurgery;
    private String surgery;
    private double roomFee;
    private double surgeonFee;
    private double supplies;

    public SurgicalBean(int id, int patientId, Timestamp dateOfSurgery, String surgery, double roomFee, double surgeonFee, double supplies) {
        this.id = id;
        this.patientId = patientId;
        this.dateOfSurgery = dateOfSurgery;
        this.surgery = surgery;
        this.roomFee = roomFee;
        this.surgeonFee = surgeonFee;
        this.supplies = supplies;
    }

    public SurgicalBean() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public Timestamp getDateOfSurgery() {
        return dateOfSurgery;
    }

    public void setDateOfSurgery(Timestamp dateOfSurgery) {
        this.dateOfSurgery = dateOfSurgery;
    }

    public String getSurgery() {
        return surgery;
    }

    public void setSurgery(String surgery) {
        this.surgery = surgery;
    }

    public double getRoomFee() {
        return roomFee;
    }

    public void setRoomFee(double roomFee) {
        this.roomFee = roomFee;
    }

    public double getSurgeonFee() {
        return surgeonFee;
    }

    public void setSurgeonFee(double surgeonFee) {
        this.surgeonFee = surgeonFee;
    }

    public double getSupplies() {
        return supplies;
    }

    public void setSupplies(double supplies) {
        this.supplies = supplies;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.id;
        hash = 29 * hash + this.patientId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SurgicalBean other = (SurgicalBean) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.patientId != other.patientId) {
            return false;
        }
        if (!Objects.equals(this.dateOfSurgery, other.dateOfSurgery)) {
            return false;
        }
        if (!Objects.equals(this.surgery, other.surgery)) {
            return false;
        }
        if (Double.doubleToLongBits(this.roomFee) != Double.doubleToLongBits(other.roomFee)) {
            return false;
        }
        if (Double.doubleToLongBits(this.surgeonFee) != Double.doubleToLongBits(other.surgeonFee)) {
            return false;
        }
        if (Double.doubleToLongBits(this.supplies) != Double.doubleToLongBits(other.supplies)) {
            return false;
        }
        return true;
    }

   

    @Override
    public String toString() {
        return "SurgicalBean{" + "id=" + id + ", patientId=" + patientId + ", dateOfSurgery=" + dateOfSurgery + ", surgery=" + surgery + ", roomFee=" + roomFee + ", surgeonFee=" + surgeonFee + ", supplies=" + supplies + '}';
    }
    
    
}
