/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jraps;

import GameAnySeven.AnySevenImpl;
import Bankroll.Bankroll;
/**
 * Execute Any7 gamble
 * @author cha_xi
 */
public class JRapsAnySeven extends JRaps {
    private final AnySevenImpl gameAnySeven;
    
    public JRapsAnySeven(){
        super();
        gameAnySeven = new AnySevenImpl();
    }
    
    public JRapsAnySeven(Bankroll ac){
        super(ac);      
        gameAnySeven = new AnySevenImpl();
    }
    @Override
    double changeMoney() {
         switch (gameAnySeven.getResult()) {
            case 4:
                return 4*super.getBet();
            case -1:
                return -super.getBet();
            default:
                return 0;
        }
    }
    
    public void perform() {
        double depositMoney = 0;
        double betOnce = 0;
        // ask customer to make a deposit;
        System.out.println("--------bank account management------");
        System.out.println("Money left: "+this.getBankAccount().getFormattedMoney());
        System.out.println("want to make deposit, input amount you want to deposit, else input 0");
        depositMoney = super.inputNonNegativeNumber();
        System.out.println("---------End of bank account management--------------");
        //finish deposit and check total residual
        System.out.println("after deposit, your total money is "+
                this.depositAccount(depositMoney));    
        //ask customer to input a bet
        System.out.println("make a bet now");
        do{betOnce = super.inputPositiveNumber();}
        while(!super.setBet(betOnce));
        System.out.println("-------Dice Rolling now, game result-------");

        gameAnySeven.GameRun();
        //check dice numbers
        System.out.println(gameAnySeven.toString());
        //change bankroll after one turn
        System.out.println("now you have "+
                super.changeAccount(this.changeMoney()));
    }    
    /**
     * run any7 gamble
     * @param args 
     */
      /* public static void main(String[] args) {
        JRapsAnySeven play = new JRapsAnySeven();
        
        play.perform();
                
    }   */  
}
