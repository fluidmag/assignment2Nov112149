/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jraps;

import Bankroll.Bankroll;
import GamePassLine.PassLineImpl;
import java.util.Scanner;

/**
 *JRaps is for control bank account during all JRaps gambling. 
 * @author cha_xi
 */
abstract class JRaps {
    private Bankroll bankAccount;
 //   private final PassLineImpl gamePassLine;
    private double bet;

    public JRaps(Bankroll bankAccount) {
        this.bankAccount = bankAccount;
        bet = 0;
    }

    public JRaps() {
        bankAccount = new Bankroll(0);
        bet = 0;
    }
    
    /**
     * decide the amount of change in PassLine after each turn
     * @return 
     */
    abstract double changeMoney();

    public double getBet() {
        return bet;
    }
/**
 * set bet and check sufficiency of bank account
 * @param bet
 * @return 
 */
    public boolean setBet(double bet) {
        if((bet>0)&(bet<=bankAccount.getMoney()))
        {
            this.bet = bet;
            return true;
        }
        else if(bet>bankAccount.getMoney())
        {
            System.out.println("WARNING:bet="+bet
                    +" > "+bankAccount.getFormattedMoney());
        }
        else
        {
            System.out.println("WARNING:bet="+bet+" <=0");
        }
        return false;
    }
/**
 * make a deposit
 * @param deposit
 * @return 
 */    
    public double depositAccount(double deposit)
    {
        bankAccount.add(deposit);
        return bankAccount.getMoney();
    }
/**
 * change Account after each run;
 * @param changeAmount
 * @return 
 */
    public double changeAccount(double changeAmount)
    {
        bankAccount.change(changeAmount);
        return bankAccount.getMoney();
    }
    /**
     * input a positive number
     * this method will be used several times,
     * @return 
     */
    public double inputPositiveNumber()
    {
            double number=0;
            String str = "input a positive number, please";
            Scanner sc = new Scanner(System.in);
            while (number <=0) {
            System.out.println(str);           
            while (!sc.hasNextDouble()){ 
                System.out.println("not valid input, input again");
                sc = new Scanner(System.in);
            }
            number = sc.nextDouble();
            str = "not Positive number, input again";          
        }
            return number;
    }
    
    public double inputNonNegativeNumber()
    {
            double number=-1;
            String str = "input a nonNegative number, please";
            Scanner sc = new Scanner(System.in);
            while (number<0) {
            System.out.println(str);           
            while (!sc.hasNextDouble()){ 
                System.out.println("not valid input, input again");
                sc = new Scanner(System.in);
            }
            number = sc.nextDouble();
            str = "not nonNegative number, input again";          
        }
            return number;
    }
    
    public Bankroll getBankAccount()
    {
        return this.bankAccount;
    }
    
}
