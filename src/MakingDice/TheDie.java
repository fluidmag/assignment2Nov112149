/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MakingDice;

import java.util.Random;

/**
 *
 * @author cha_xi
 */
public class TheDie {
    private int oneDie;

    public TheDie() {
    }

    public int getOneDie() {
        return oneDie;
    }

    
    public void rollTheDie(){
        Random rand = new Random();
        oneDie = rand.nextInt(6) + 1;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.oneDie;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TheDie other = (TheDie) obj;
        if (this.oneDie != other.oneDie) {
            return false;
        }
        return true;
    }
    
    
}
