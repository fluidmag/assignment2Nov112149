/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Constants;

/**
 *
 * @author cha_xi
 */
public class JRapsConstants {
    private JRapsConstants(){}
    public static final int PassLineFirstTurnWinNum = 7;
    public static final int PassLineOtherTurnLoseNum = 7;
    public static final int [] PassLineFirstTurnLoseNum= {2,3,12};
    public static final int [] FieldBetWinNum = {3,4,9,10,11};
    public static final int FieldBetDoubleWinNum = 2;
    public static final int FieldBetTripleWinNum = 12;
    public static final int [] FieldBetLoseNum = {5,6,7,8};
    public static final int AnySevenWinNum = 7;
    public static final int [] AnySevenLoseNum = {2,3,4,5,6,8,9,10,11,12};    
}
